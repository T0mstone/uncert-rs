#![cfg_attr(not(feature = "std"), no_std)]

use core::ops::{Add, Div, Mul, Neg, Sub};
use crate::uncertainty::markers::IsCoUnc;

/// Types for the number two and its inverse.
pub mod two;
pub mod uncertainty;
/// First order error propagation (probably what you want).
pub mod linear;

/// An exactly known value
#[derive(Default, Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
#[repr(transparent)]
pub struct Exact<T>(pub T);

impl<T: Neg> Neg for Exact<T> {
	type Output = Exact<T::Output>;

	fn neg(self) -> Self::Output {
		Exact(-self.0)
	}
}

macro_rules! impl_binops_exact {
	($($Tr:ident::$meth:ident),*) => {
		$(impl<T: $Tr<U>, U> $Tr<Exact<U>> for Exact<T> {
			type Output = Exact<T::Output>;

			fn $meth(self, rhs: Exact<U>) -> Self::Output {
				Exact($Tr::$meth(self.0, rhs.0))
			}
		})*
	};
}

impl_binops_exact!(Add::add, Sub::sub, Mul::mul, Div::div);

/// An uncorrelated value.
///
/// This is used as part of a binary operation to express that the two operands are uncorrelated
#[derive(Default, Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
#[repr(transparent)]
pub struct Uncorrelated<T>(pub T);

/// A correlated value.
///
/// This is used as part of a binary operation to express that the two operands are correlated,
/// and to specify their co-uncertainty.
#[derive(Default, Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct Correlated<T, C: IsCoUnc> {
	pub value: T,
	/// The co-uncertainty between this value and another value.
	pub counc: C,
}

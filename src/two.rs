use core::ops::Mul;

use tstd::hlist::HList;
use tstd::ops::binary_op::traits::Pow;
use tstd::sym_ops::{Operation, Ready};

/// The number `2`.
#[derive(Default, Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct Two;

/// The number `0.5`.
///
/// `Pow<EOneHalf>` represents the positive square root operation.
#[derive(Default, Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct OneHalf;

macro_rules! delegate_lazy {
    (impl[$($gen:tt)*] $Op:ident<$Rhs:ty> for $t:ty { fn $meth:ident; }) => {
		impl<$($gen)*> $Op<$Rhs> for $t {
			type Output = <Self as $Op<Ready<$Rhs>>>::Output;

			fn $meth(self, rhs: $Rhs) -> Self::Output {
				self.$meth(Ready(rhs))
			}
		}
	};
}
delegate_lazy!(impl[T] Pow<Two> for Ready<T> { fn pow; });
delegate_lazy!(impl[T] Pow<OneHalf> for Ready<T> { fn pow; });

delegate_lazy!(impl[O, Args: HList] Pow<Two> for Operation<O, Args> { fn pow; });
delegate_lazy!(impl[O, Args: HList] Pow<OneHalf> for Operation<O, Args> { fn pow; });

#[cfg(any(feature = "num-traits-libm", feature = "std"))]
const _IMPL_SQRT: () = {
	#[cfg(feature = "std")]
	macro_rules! sqrt {
		($x:expr) => {
			$x.sqrt()
		};
	}
	#[cfg(feature = "num-traits-libm")]
	macro_rules! sqrt {
		($x:expr) => {
			num_traits::Float::sqrt($x)
		};
	}

	impl Pow<OneHalf> for f32 {
		type Output = f32;

		fn pow(self, _: OneHalf) -> Self::Output {
			sqrt!(self)
		}
	}
	impl Pow<OneHalf> for f64 {
		type Output = f64;

		fn pow(self, _: OneHalf) -> Self::Output {
			sqrt!(self)
		}
	}
};

#[cfg(feature = "std")]
macro_rules! square {
	($x:expr) => {
		$x.powi(2)
	};
}
#[cfg(feature = "num-traits-libm")]
macro_rules! square {
	($x:expr) => {
		num_traits::Float::powi($x, 2)
	};
}
#[cfg(not(any(feature = "num-traits-libm", feature = "std")))]
macro_rules! square {
	($x:expr) => {
		$x * $x
	};
}

impl Pow<Two> for f32 {
	type Output = f32;

	fn pow(self, _: Two) -> Self::Output {
		square!(self)
	}
}
impl Pow<Two> for f64 {
	type Output = f64;

	fn pow(self, _: Two) -> Self::Output {
		square!(self)
	}
}

impl Mul<Two> for f32 {
	type Output = f32;

	fn mul(self, _: Two) -> Self::Output {
		self * 2.0
	}
}
impl Mul<Two> for f64 {
	type Output = f64;

	fn mul(self, _: Two) -> Self::Output {
		self * 2.0
	}
}

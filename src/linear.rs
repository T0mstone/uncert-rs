use core::ops::{Add, Div, Mul, Neg, Sub};

#[cfg(feature = "num-traits")]
use num_traits::Pow;
#[cfg(not(feature = "num-traits"))]
use tstd::ops::binary_op::traits::Pow;
use tstd::ops::unary_op::traits::Inv;
use tstd::ops::{binary_op as bop, unary_op as uop, Operation};
use tstd::sym_ops::{BinOp, Eval, Ready, UnOp};
use tstd::{hlist, HList};

use crate::two::Two;
use crate::uncertainty::markers::{CoUncKind, IsCoUnc, IsSelfUnc, SelfUncKind};
use crate::uncertainty::{Absolute, Co, ConvertTo, Relative, Squared, Uncertainty};
use crate::{Correlated, Exact, Uncorrelated};

/// An uncertain value that propagates uncertainty linearly.
#[derive(Default, Debug, Copy, Clone)]
pub struct Uncertain<V, U: IsSelfUnc> {
	pub value: V,
	pub uncer: U,
}

type LazyClone<'a, T> = UnOp<uop::Clone, Ready<&'a T>>;
fn lazy_clone<T>(val: &T) -> LazyClone<T> {
	LazyClone::new1(Ready(val))
}

pub trait ConvertUncTo<K: SelfUncKind, V> {
	type OutValue;

	fn convert(self, of: &V) -> Uncertainty<Ready<Self::OutValue>, K>;
}
impl<U: IsSelfUnc, K: SelfUncKind, V, UK> ConvertUncTo<K, V> for U
where
	for<'a> HList![U, LazyClone<'a, V>]: Operation<ConvertTo<K>>,
	for<'a> <HList![U, LazyClone<'a, V>] as Operation<ConvertTo<K>>>::Output: Eval<Output = UK>,
{
	type OutValue = UK;

	fn convert(self, of: &V) -> Uncertainty<Ready<Self::OutValue>, K> {
		Ready(Operation::op(hlist![self, lazy_clone(of)]).eval()).into()
	}
}
pub trait ConvertCoUncTo<K: CoUncKind, V1, U1: IsSelfUnc, V2, U2: IsSelfUnc> {
	type OutValue;

	fn convert(
		self,
		of1: &Uncertain<V1, U1>,
		of2: &Uncertain<V2, U2>,
	) -> Uncertainty<Ready<Self::OutValue>, K>;
}
impl<U: IsCoUnc, K: CoUncKind, V1, U1: IsSelfUnc, V2, U2: IsSelfUnc, UK>
	ConvertCoUncTo<K, V1, U1, V2, U2> for U
where
	for<'a> HList![
		U,
		LazyClone<'a, V1>,
		LazyClone<'a, U1>,
		LazyClone<'a, V2>,
		LazyClone<'a, U2>
	]: Operation<ConvertTo<K>>,
	for<'a> <HList![
		U,
		LazyClone<'a, V1>,
		LazyClone<'a, U1>,
		LazyClone<'a, V2>,
		LazyClone<'a, U2>
	] as Operation<ConvertTo<K>>>::Output: Eval<Output = UK>,
{
	type OutValue = UK;

	fn convert(
		self,
		of1: &Uncertain<V1, U1>,
		of2: &Uncertain<V2, U2>,
	) -> Uncertainty<Ready<Self::OutValue>, K> {
		Ready(
			Operation::op(hlist![
				self,
				lazy_clone(&of1.value),
				lazy_clone(&of1.uncer),
				lazy_clone(&of2.value),
				lazy_clone(&of2.uncer)
			])
			.eval(),
		)
		.into()
	}
}

impl<V, U: IsSelfUnc> Uncertain<V, U> {
	#[inline]
	pub fn with_uncer_as<K: SelfUncKind>(self) -> Uncertain<V, Uncertainty<Ready<U::OutValue>, K>>
	where
		U: ConvertUncTo<K, V>,
	{
		let uncer = self.uncer.convert(&self.value);
		Uncertain {
			value: self.value,
			uncer,
		}
	}
}

impl<V, U: IsSelfUnc> Neg for Uncertain<V, U>
where
	V: Neg,
{
	type Output = Uncertain<V::Output, U>;

	fn neg(self) -> Self::Output {
		Uncertain {
			value: -self.value,
			uncer: self.uncer,
		}
	}
}

impl<V, U: IsSelfUnc> Inv for Uncertain<V, U>
where
	V: Inv,
	U: ConvertUncTo<Squared<Relative>, V>,
{
	type Output = Uncertain<V::Output, Uncertainty<Ready<U::OutValue>, Squared<Relative>>>;

	fn inv(self) -> Self::Output {
		// note: the *relative* uncertainty stays the same (not the absolute), hence the need to convert
		let Uncertain { value, uncer: usq } = self.with_uncer_as::<Squared<Relative>>();
		Uncertain {
			value: value.inv(),
			uncer: usq,
		}
	}
}

impl<V, U: IsSelfUnc, T> Pow<T> for Uncertain<V, U>
where
	V: Pow<T>,
	T: Clone,
	U: ConvertUncTo<Squared<Relative>, V>,
{
	type Output = Uncertain<
		V::Output,
		Uncertainty<BinOp<bop::Mul, Ready<U::OutValue>, Ready<T>>, Squared<Relative>>,
	>;

	fn pow(self, exp: T) -> Self::Output {
		let Uncertain { value, uncer: usq } = self.with_uncer_as::<Squared<Relative>>();
		Uncertain {
			value: value.pow(exp.clone()),
			uncer: (usq.value * Ready(exp)).into(),
		}
	}
}

macro_rules! impl_binops_uncorr {
    ($($Tr:ident::$meth:ident => $Kind:ident),*) => {
		$(impl<V1, V2, U1: IsSelfUnc, U2: IsSelfUnc> $Tr<Uncorrelated<Uncertain<V2, U2>>> for Uncertain<V1, U1>
		where
			V1: $Tr<V2>,
			U1: ConvertUncTo<Squared<$Kind>, V1>,
			U2: ConvertUncTo<Squared<$Kind>, V2>,
		{
			type Output = Uncertain<V1::Output, Uncertainty<BinOp<bop::Add, Ready<U1::OutValue>, Ready<U2::OutValue>>, Squared<$Kind>>>;

			fn $meth(self, Uncorrelated(rhs): Uncorrelated<Uncertain<V2, U2>>) -> Self::Output {
				let Uncertain { value: lhs, uncer: usq1 } = self.with_uncer_as::<Squared<$Kind>>();
				let Uncertain { value: rhs, uncer: usq2 } = rhs.with_uncer_as::<Squared<$Kind>>();
				Uncertain {
					value: $Tr::$meth(lhs, rhs),
					#[allow(clippy::suspicious_arithmetic_impl)]
					uncer: (usq1.value + usq2.value).into(),
				}
			}
		})*
	};
}

impl_binops_uncorr!(
	Add::add => Absolute,
	Sub::sub => Absolute,
	Mul::mul => Relative,
	Div::div => Relative
);

macro_rules! impl_binops_corr {
    ($($Tr:ident::$meth:ident => $Kind:ident, $Unc:ident::$unc:ident),*) => {
		$(impl<V1, V2, U1: IsSelfUnc, U2: IsSelfUnc, U12: IsCoUnc> $Tr<Correlated<Uncertain<V2, U2>, U12>> for Uncertain<V1, U1>
		where
			V1: $Tr<V2>,
			U1: ConvertUncTo<Squared<$Kind>, V1>,
			U2: ConvertUncTo<Squared<$Kind>, V2>,
			U12: ConvertCoUncTo<Co<$Kind, $Kind>, V1, U1, V2, U2>,
		{
			type Output = Uncertain<
				V1::Output,
				Uncertainty<
					BinOp<
						bop::$Unc,
						BinOp<bop::Add, Ready<U1::OutValue>, Ready<U2::OutValue>>,
						BinOp<bop::Mul, Ready<U12::OutValue>, Ready<Two>>,
					>,
					Squared<$Kind>,
				>,
			>;

			fn $meth(
				self,
				Correlated { value: rhs, counc }: Correlated<Uncertain<V2, U2>, U12>,
			) -> Self::Output {
				let counc_abs_value: Ready<U12::OutValue> = Ready(counc.convert(&self, &rhs).eval().value);

				let Uncertain {
					value: lhs,
					uncer: usq1,
				} = self.with_uncer_as::<Squared<$Kind>>();
				let Uncertain {
					value: rhs,
					uncer: usq2,
				} = rhs.with_uncer_as::<Squared<$Kind>>();

				Uncertain {
					value: $Tr::$meth(lhs, rhs),
					#[allow(clippy::suspicious_arithmetic_impl)]
					uncer: $Unc::$unc((usq1.value + usq2.value), counc_abs_value * Ready(Two)).into(),
				}
			}
		})*
	};
}

impl_binops_corr!(
	Add::add => Absolute, Add::add,
	Sub::sub => Absolute, Sub::sub,
	Mul::mul => Relative, Add::add,
	Div::div => Relative, Sub::sub
);

macro_rules! impl_addsub_exact {
    ($($Tr:ident::$meth:ident),*) => {
		$(impl<V1, V2, U: IsSelfUnc> $Tr<Exact<V2>> for Uncertain<V1, U>
		where
			V1: $Tr<V2>,
		{
			type Output = Uncertain<V1::Output, U>;

			fn $meth(self, rhs: Exact<V2>) -> Self::Output {
				Uncertain {
					value: $Tr::$meth(self.value, rhs.0),
					uncer: self.uncer,
				}
			}
		}

		impl<V1, V2, U: IsSelfUnc> $Tr<Uncertain<V2, U>> for Exact<V1>
		where
			V1: $Tr<V2>,
		{
			type Output = Uncertain<V1::Output, U>;

			fn $meth(self, rhs: Uncertain<V2, U>) -> Self::Output {
				Uncertain {
					value: $Tr::$meth(self.0, rhs.value),
					uncer: rhs.uncer,
				}
			}
		})*
	};
}

impl_addsub_exact!(Add::add, Sub::sub);

impl<V1, V2, U: IsSelfUnc> Mul<Exact<V2>> for Uncertain<V1, U>
where
	V1: Mul<V2>,
	V2: Clone,
	U: Mul<V2>,
	U::Output: IsSelfUnc,
{
	type Output = Uncertain<V1::Output, U::Output>;

	fn mul(self, rhs: Exact<V2>) -> Self::Output {
		Uncertain {
			value: self.value * rhs.0.clone(),
			uncer: self.uncer * rhs.0,
		}
	}
}

impl<V1, V2, U: IsSelfUnc> Mul<Uncertain<V2, U>> for Exact<V1>
where
	V1: Clone + Mul<V2> + Mul<U>,
	<V1 as Mul<U>>::Output: IsSelfUnc,
{
	type Output = Uncertain<<V1 as Mul<V2>>::Output, <V1 as Mul<U>>::Output>;

	fn mul(self, rhs: Uncertain<V2, U>) -> Self::Output {
		Uncertain {
			value: self.0.clone() * rhs.value,
			uncer: self.0 * rhs.uncer,
		}
	}
}

impl<V1, V2, U: IsSelfUnc> Div<Exact<V2>> for Uncertain<V1, U>
where
	V1: Div<V2>,
	V2: Clone,
	U: Div<V2>,
	U::Output: IsSelfUnc,
{
	type Output = Uncertain<V1::Output, U::Output>;

	fn div(self, rhs: Exact<V2>) -> Self::Output {
		Uncertain {
			value: self.value / rhs.0.clone(),
			uncer: self.uncer / rhs.0,
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::uncertainty::Correlation;

	#[test]
	fn sqrt_absolute_lazy() {
		let a = Uncertain {
			value: BinOp::<tstd::ops::binary_op::Add, Ready<f64>, Ready<f64>>::new2(
				Ready(1.0),
				Ready(2.0),
			),
			uncer: Uncertainty::<_, Squared<Absolute>>::new(1.0),
		};
		let b = a.clone().with_uncer_as::<Absolute>();

		let a_uncer: f64 = a.uncer.value.eval();
		let b_uncer: f64 = b.uncer.value.eval();
		assert!((a_uncer - b_uncer).abs() <= f64::EPSILON);
	}

	#[test]
	fn addf64_uncorr() {
		let a = Uncertain {
			value: 1.0f64,
			uncer: Uncertainty::<_, Absolute>::new(1.0),
		};
		let b = Uncertain {
			value: 2.0f64,
			uncer: Uncertainty::<_, Absolute>::new(2.0),
		};

		let c = a + Uncorrelated(b);
		let Uncertain { value, uncer } = c.with_uncer_as::<Absolute>();
		let uncer_value: Ready<f64> = uncer.value;

		assert!((value - 3.0f64).abs() <= f64::EPSILON);
		assert!((uncer_value.0 - f64::hypot(1.0, 2.0)).abs() <= f64::EPSILON);
	}

	#[test]
	fn mulf64_uncorr() {
		let a = Uncertain {
			value: 1.0f64,
			uncer: Uncertainty::<_, Relative>::new(1.0),
		};
		let b = Uncertain {
			value: 2.0f64,
			uncer: Uncertainty::<_, Relative>::new(2.0),
		};

		let c = a * Uncorrelated(b);
		let Uncertain { value, uncer } = c.with_uncer_as::<Relative>();
		let uncer_value: Ready<f64> = uncer.value;

		assert!((value - 2.0f64).abs() <= f64::EPSILON);
		assert!((uncer_value.0 - f64::hypot(1.0, 2.0)).abs() <= f64::EPSILON);
	}

	#[test]
	fn addf64_corr() {
		let a = Uncertain {
			value: 1.0f64,
			uncer: Uncertainty::<_, Absolute>::new(1.0),
		};
		let b = Uncertain {
			value: -2.0f64,
			uncer: Uncertainty::<_, Absolute>::new(2.0),
		};

		let c = a + Correlated {
			value: b,
			counc: Uncertainty::<_, Co<Absolute, Absolute>>::new(1.0),
		};
		let Uncertain { value, uncer } = c.with_uncer_as::<Absolute>();
		let uncer_value: Ready<f64> = uncer.value;

		assert!((value - -1.0f64).abs() <= f64::EPSILON);
		assert!(
			(uncer_value.0 - (1.0f64 + 4.0 + 2.0).sqrt()).abs() <= f64::EPSILON,
			"{}",
			uncer_value.0
		);
	}

	#[test]
	fn subf64_corr() {
		let a = Uncertain {
			value: 1.0f64,
			uncer: Uncertainty::<_, Absolute>::new(1.0),
		};
		let b = Uncertain {
			value: -2.0f64,
			uncer: Uncertainty::<_, Absolute>::new(2.0),
		};

		let c = a - Correlated {
			value: b,
			counc: Uncertainty::<_, Co<Absolute, Absolute>>::new(1.0),
		};
		let Uncertain { value, uncer } = c.with_uncer_as::<Absolute>();
		let uncer_value: Ready<f64> = uncer.value;

		assert!((value - 3.0f64).abs() <= f64::EPSILON);
		assert!(
			(uncer_value.0 - (1.0f64 + 4.0 - 2.0).sqrt()).abs() <= f64::EPSILON,
			"{}",
			uncer_value.0
		);
	}

	#[test]
	fn mulf64_corr() {
		let a = Uncertain {
			value: 1.0f64,
			uncer: Uncertainty::<_, Relative>::new(1.0),
		};
		let b = Uncertain {
			value: -2.0f64,
			uncer: Uncertainty::<_, Relative>::new(2.0),
		};

		let c = a * Correlated {
			value: b,
			counc: Uncertainty::<_, Co<Relative, Relative>>::new(1.0),
		};
		let Uncertain { value, uncer } = c.with_uncer_as::<Relative>();
		let uncer_value: Ready<f64> = uncer.value;

		assert!((value - -2.0f64).abs() <= f64::EPSILON);
		assert!(
			(uncer_value.0 - (1.0f64 + 4.0 + 2.0 * 1.0).sqrt()).abs() <= f64::EPSILON,
			"{}",
			uncer_value.0
		);
	}

	#[test]
	fn neg_counc() {
		let a = Uncertain {
			value: 1.0f64,
			uncer: Uncertainty::<_, Relative>::new(1.0),
		};
		let b = Uncertain {
			value: -2.0f64,
			uncer: Uncertainty::<_, Relative>::new(2.0),
		};

		let c = a.mul(Correlated {
			value: b,
			counc: Uncertainty::<_, Co<Absolute, Absolute>>::new(2.0),
		});
		let Uncertain { value: _, uncer } = c.with_uncer_as::<Relative>();
		let uncer_value: Ready<f64> = uncer.value;

		assert!(
			(uncer_value.0 - (1.0f64 + 4.0 - 2.0 * 1.0).sqrt()).abs() <= 2.0 * f64::EPSILON,
			"{}",
			uncer_value.0
		);
	}

	#[test]
	fn corr_coeff() {
		let a = Uncertain {
			value: 1.0f64,
			uncer: Uncertainty::<_, Absolute>::new(1.0f64),
		};
		let b = Uncertain {
			value: 2.0f64,
			uncer: Uncertainty::<_, Absolute>::new(2.0f64),
		};

		let corr = Uncertainty::<_, Correlation>::new(1.0f64);

		let counc: Uncertainty<Ready<_>, Co<Absolute, Absolute>> = corr.convert(&a, &b);

		assert!((counc.value.eval() - 2.0).abs() <= f64::EPSILON);
	}
}

use core::cmp::Ordering;
use core::fmt::{Debug, Formatter};
use core::hash::{Hash, Hasher};
use core::marker::PhantomData;
use core::ops::{Div, Mul};

use tstd::ops::binary_op::traits::Pow;
use tstd::ops::Operation;
use tstd::sym_ops::{Eval, Ready};
use tstd::{hlist, hlist_pat, HList};

use crate::two::{OneHalf, Two};
use crate::uncertainty::markers::{CoUncKind, IsCoUnc, IsSelfUnc, SelfUncKind};

/// Marker traits.
pub mod markers {
	pub trait SelfUncKind {}
	pub trait CoUncKind {}

	pub trait IsSelfUnc {}
	pub trait IsCoUnc {}
}

/// Marker for an absolute uncertainty.
#[derive(Default, Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct Absolute;

/// Marker for a relative uncertainty.
#[derive(Default, Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct Relative;

/// Marker for the square of an uncertainty.
#[derive(Default, Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
#[repr(transparent)]
pub struct Squared<M>(pub M);

impl SelfUncKind for Absolute {}
impl SelfUncKind for Relative {}
impl<M: SelfUncKind> SelfUncKind for Squared<M> {}

/// Marker for a co-uncertainty.
///
/// Co-uncertainty is to covariance as uncertainty is to variance.
/// Here, uncertainty is also called self-uncertainty to distinguish it from co-uncertainty.
///
/// List of values:
/// - `Co<Absolute, Absolute>`: Absolute co-uncertainty,
/// - `Co<Relative, Relative>`: Relative co-uncertainty (relative to the product of the two correlated values)
/// - `Co<Absolute, Relative>`, `Co<Relative, Absolute>`: Mixed Absolute/Relative co-uncertainty (only relative to one of the two correlated values)
#[derive(Default, Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct Co<L, R>(pub L, pub R);

/// Marker for representing co-uncertainty by the correlation coefficient.
///
/// The formula for this is to divide the absolute co-uncertainty by the product of the non-squared absolute self-uncertainties of the two variables.
#[derive(Default, Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct Correlation;

impl<L: SelfUncKind, R: SelfUncKind> CoUncKind for Co<L, R> {}
impl CoUncKind for Correlation {}

/// An uncertainty of a given kind.
#[repr(transparent)]
pub struct Uncertainty<T, U> {
	/// The numeric value of the uncertainty
	pub value: T,
	/// Which kind of uncertainty the value represents
	pub kind: PhantomData<U>,
}

impl<T, U> IsSelfUnc for Uncertainty<T, U> where U: SelfUncKind {}
impl<T, U> IsCoUnc for Uncertainty<T, U> where U: CoUncKind {}

impl<T: Default, U> Default for Uncertainty<T, U> {
	fn default() -> Self {
		Self {
			value: T::default(),
			kind: PhantomData,
		}
	}
}

impl<T: Debug, U> Debug for Uncertainty<T, U> {
	fn fmt(&self, f: &mut Formatter) -> core::fmt::Result {
		f.debug_struct("Uncertainty")
			.field("value", &self.value)
			.field("kind", &core::any::type_name::<U>())
			.finish()
	}
}

impl<T: Clone, U> Clone for Uncertainty<T, U> {
	fn clone(&self) -> Self {
		Self {
			value: self.value.clone(),
			kind: PhantomData,
		}
	}
}
impl<T: Copy, U> Copy for Uncertainty<T, U> {}

impl<T: Ord, U> Ord for Uncertainty<T, U> {
	fn cmp(&self, other: &Self) -> Ordering {
		self.value.cmp(&other.value)
	}
}
impl<T: PartialOrd, U> PartialOrd for Uncertainty<T, U> {
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		self.value.partial_cmp(&other.value)
	}
}
impl<T: Eq, U> Eq for Uncertainty<T, U> {}
impl<T: PartialEq, U> PartialEq for Uncertainty<T, U> {
	fn eq(&self, other: &Self) -> bool {
		self.value.eq(&other.value)
	}
}

impl<T: Hash, U> Hash for Uncertainty<T, U> {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.value.hash(state);
	}
}

impl<T, U> From<T> for Uncertainty<T, U> {
	fn from(value: T) -> Self {
		Self {
			value,
			kind: PhantomData,
		}
	}
}

impl<T, U> Uncertainty<Ready<T>, U> {
	pub fn new(value: T) -> Self {
		Self {
			value: Ready(value),
			kind: PhantomData,
		}
	}
}

impl<T: Eval, U> Eval for Uncertainty<T, U> {
	type Output = Uncertainty<T::Output, U>;

	fn eval(self) -> Uncertainty<T::Output, U> {
		self.value.eval().into()
	}
}

pub type AbsoluteUncertainty<T> = Uncertainty<T, Absolute>;
pub type RelativeUncertainty<T> = Uncertainty<T, Relative>;
pub type AbsoluteUncertaintySquared<T> = Uncertainty<T, Squared<Absolute>>;
pub type RelativeUncertaintySquared<T> = Uncertainty<T, Squared<Relative>>;

impl<U> Pow<OneHalf> for Squared<U> {
	type Output = U;

	fn pow(self, _: OneHalf) -> Self::Output {
		self.0
	}
}
impl Pow<Two> for Absolute {
	type Output = Squared<Self>;

	fn pow(self, _: Two) -> Self::Output {
		Squared(self)
	}
}
impl Pow<Two> for Relative {
	type Output = Squared<Self>;

	fn pow(self, _: Two) -> Self::Output {
		Squared(self)
	}
}

pub struct ConvertTo<To>(PhantomData<To>);

impl<O, V1, V2, U1, U2> Operation<ConvertTo<U2>> for HList![Uncertainty<V1, U1>, O]
where
	HList![V1, O]: Operation<ConvertUncertaintyValue<U1, U2>, Output = V2>,
{
	type Output = V2;

	fn op(self) -> Self::Output {
		let hlist_pat![unc, of] = self;
		hlist![unc.value, of].op()
	}
}

impl<O1, OU1, O2, OU2, V1, V2, U1, U2> Operation<ConvertTo<U2>> for HList![Uncertainty<V1, U1>, O1, OU1, O2, OU2]
where
	HList![V1, O1, OU1, O2, OU2]: Operation<ConvertUncertaintyValue<U1, U2>, Output = V2>,
{
	type Output = V2;

	fn op(self) -> Self::Output {
		let hlist_pat![unc, of1, unc1, of2, unc2] = self;
		hlist![unc.value, of1, unc1, of2, unc2].op()
	}
}

struct ConvertUncertaintyValue<From, To>(PhantomData<(From, To)>);

macro_rules! conv_unc_val {
    (
		$({$($gen:tt)*})? $From:ty => $To:ty;
		$T:ident, $O:ident -> $Out:ty
		$(where {$($wheres:tt)*})?;
		|$unc:ident, $of:ident| $res:expr
	) => {
		impl<$T, $O, $($($gen)*)?> Operation<ConvertUncertaintyValue<$From, $To>> for HList![$T, $O]
		$(where $($wheres)*)?
		{
			type Output = $Out;

			fn op(self) -> Self::Output {
				let hlist_pat![$unc, $of] = self;
				$res
			}
		}
	};
}

// identity
conv_unc_val!(
	{U} U => U;
	T, _O -> T;
	|unc, _of| unc
);

// square and square root
conv_unc_val!(
	{U} U => Squared<U>;
	T, _O -> T::Output
	where { T: Pow<Two> };
	|unc, _of| unc.pow(Two)
);
conv_unc_val!(
	{U} Squared<U> => U;
	T, _O -> T::Output
	where { T: Pow<OneHalf> };
	|unc, _of| unc.pow(OneHalf)
);

// core conversions (multiply/divide)
conv_unc_val!(
	Absolute => Relative;
	T, O -> T::Output
	where { T: Div<O> };
	|unc, of| unc / of
);
conv_unc_val!(
	Relative => Absolute;
	R, O -> R::Output
	where { R: Mul<O> };
	|unc, of| unc * of
);
conv_unc_val!(
	Squared<Absolute> => Squared<Relative>;
	T2, O -> T2::Output
	where {
		O: Pow<Two>,
		T2: Div<O::Output>
	};
	|unc, of| unc / of.pow(Two)
);
conv_unc_val!(
	Squared<Relative> => Squared<Absolute>;
	R2, O -> R2::Output
	where {
		O: Pow<Two>,
		R2: Mul<O::Output>
	};
	|unc, of| unc * of.pow(Two)
);

// combined conversions
conv_unc_val!(
	Absolute => Squared<Relative>;
	T, O -> <T::Output as Pow<Two >>::Output
	where {
		T: Div<O>,
		T::Output: Pow<Two>
	};
	|unc, of| (unc / of).pow(Two)
);
conv_unc_val!(
	Relative => Squared<Absolute>;
	R, O -> <R::Output as Pow<Two >>::Output
	where {
		R: Mul<O>,
		R::Output: Pow<Two>
	};
	|unc, of| (unc * of).pow(Two)
);
conv_unc_val!(
	Squared<Absolute> => Relative;
	T2, O -> <T2::Output as Div<O>>::Output
	where {
		T2: Pow<OneHalf>,
		T2::Output: Div<O>
	};
	|unc, of| unc.pow(OneHalf) / of
);
conv_unc_val!(
	Squared<Relative> => Absolute;
	R2, O -> <R2::Output as Mul<O>>::Output
	where {
		R2: Pow<OneHalf>,
		R2::Output: Mul<O>
	};
	|unc, of| unc.pow(OneHalf) * of
);

impl<T2, T2K3, T2K34, K1, K2, K3, K4, O1, O2, _U1, _U2>
	Operation<ConvertUncertaintyValue<Co<K1, K2>, Co<K3, K4>>> for HList![T2, O1, _U1, O2, _U2]
where
	HList![T2, O1]: Operation<ConvertUncertaintyValue<K1, K3>, Output = T2K3>,
	HList![T2K3, O2]: Operation<ConvertUncertaintyValue<K2, K4>, Output = T2K34>,
{
	type Output = T2K34;

	fn op(self) -> Self::Output {
		let hlist_pat![unc, of1, _unc1, of2, _unc2] = self;
		Operation::op(hlist![Operation::op(hlist![unc, of1]), of2])
	}
}

// maybe-todo: make these non-`Eval`-dependent
// (since U1 and U2 are passed in as lazy values, the demand that e.g. `HList[U1, O1]: ConvertTo<Absolute>` isn't really possible normally)

impl<T2, T2L, T2A, K1, K2, O1: Clone + Eval, O2: Clone + Eval, U1: Eval, U2: Eval, U1A, U2A>
	Operation<ConvertUncertaintyValue<Co<K1, K2>, Correlation>> for HList![T2, O1, U1, O2, U2]
where
	HList![T2, O1::Output]: Operation<ConvertUncertaintyValue<K1, Absolute>, Output = T2L>,
	HList![T2L, O2::Output]: Operation<ConvertUncertaintyValue<K2, Absolute>, Output = T2A>,
	HList![U1::Output, O1::Output]: Operation<ConvertTo<Absolute>, Output = U1A>,
	HList![U2::Output, O2::Output]: Operation<ConvertTo<Absolute>, Output = U2A>,
	U1A: Mul<U2A>,
	T2A: Div<U1A::Output>,
{
	type Output = T2A::Output;

	fn op(self) -> Self::Output {
		let hlist_pat![unc, of1, unc1, of2, unc2] = self;
		let unc_abs = Operation::op(hlist![
			Operation::op(hlist![unc, of1.clone().eval()]),
			of2.clone().eval()
		]);
		let unc1_abs = Operation::op(hlist![unc1.eval(), of1.eval()]);
		let unc2_abs = Operation::op(hlist![unc2.eval(), of2.eval()]);
		unc_abs / (unc1_abs * unc2_abs)
	}
}

impl<
		R,
		T2,
		T2K1,
		T2K12,
		K1,
		K2,
		O1: Clone + Eval,
		O2: Clone + Eval,
		U1: Eval,
		U2: Eval,
		U1A,
		U2A,
	> Operation<ConvertUncertaintyValue<Correlation, Co<K1, K2>>> for HList![R, O1, U1, O2, U2]
where
	HList![U1::Output, O1::Output]: Operation<ConvertTo<Absolute>, Output = U1A>,
	HList![U2::Output, O2::Output]: Operation<ConvertTo<Absolute>, Output = U2A>,
	U1A: Mul<U2A>,
	R: Mul<U1A::Output, Output = T2>,
	HList![T2, O1::Output]: Operation<ConvertUncertaintyValue<Absolute, K1>, Output = T2K1>,
	HList![T2K1, O2::Output]: Operation<ConvertUncertaintyValue<Absolute, K2>, Output = T2K12>,
{
	type Output = T2K12;

	fn op(self) -> Self::Output {
		let hlist_pat![corr, of1, unc1, of2, unc2] = self;
		let unc1_abs = Operation::op(hlist![unc1.eval(), of1.clone().eval()]);
		let unc2_abs = Operation::op(hlist![unc2.eval(), of2.clone().eval()]);
		let unc_abs = corr * (unc1_abs * unc2_abs);
		Operation::op(hlist![
			Operation::op(hlist![unc_abs, of1.eval()]),
			of2.eval()
		])
	}
}
